package com.myzee.tablerelation.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myzee.tablerelation.entity.Employee;
import com.myzee.tablerelation.entity.EmployeeList;
import com.myzee.tablerelation.service.EmployeeService;

@RestController
@RequestMapping (value = "/employee")
public class EmployeeController {
	
	@Autowired
	EmployeeService employeeService;
	
	@PostMapping(value = "/add", consumes = "application/json")
	public String saveEmployee(@RequestBody Employee e) {
		employeeService.saveEmployee(e);
		return "employee saved successfully!!";
	}
	
	@GetMapping(value = "/list", produces = "application/json")
	public EmployeeList getEmployees() {
		return employeeService.listEmployees();
	}
}
