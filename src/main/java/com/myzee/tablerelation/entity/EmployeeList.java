package com.myzee.tablerelation.entity;

import java.util.List;

public class EmployeeList {
	
	List<Employee> empList;
	
	public EmployeeList() {
	}

	public EmployeeList(List<Employee> empList) {
		super();
		this.empList = empList;
	}

	public List<Employee> getEmpList() {
		return empList;
	}

	public void setEmpList(List<Employee> empList) {
		this.empList = empList;
	}
	
	
}
