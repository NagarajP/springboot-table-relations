package com.myzee.tablerelation.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myzee.tablerelation.entity.Employee;
import com.myzee.tablerelation.entity.EmployeeList;
import com.myzee.tablerelation.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Override
	public void saveEmployee(Employee e) {
		employeeRepository.save(e);
	}

	@Override
	public EmployeeList listEmployees() {
		EmployeeList el = new EmployeeList();
		List<Employee> empList = new ArrayList<Employee>();
		employeeRepository.findAll().forEach(emp -> empList.add(emp));
		el.setEmpList(empList);
		return el;
	}

}
