package com.myzee.tablerelation.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.myzee.tablerelation.entity.Employee;
import com.myzee.tablerelation.entity.EmployeeList;

public interface EmployeeService {
	
	public void saveEmployee(Employee e);
	public EmployeeList listEmployees();
	
}
