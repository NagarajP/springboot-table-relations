package com.myzee.tablerelation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootTableRelationsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootTableRelationsApplication.class, args);
	}

}
